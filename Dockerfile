FROM debian:stable-slim

WORKDIR /tmp

ENV AWS_VERSION="2.8.2"

RUN apt-get -y update &&\
    apt-get -y install \
      curl \
      groff \
      unzip \
      cowsay

RUN curl --silent "https://awscli.amazonaws.com/awscli-exe-linux-x86_64${AWS_VERSION:+-${AWS_VERSION}}.zip" -o "awscli.zip"
RUN unzip -DD -q awscli.zip
RUN ./aws/install
RUN aws --version

RUN echo AWS CLI changelog is here https://github.com/aws/aws-cli/blob/v2/CHANGELOG.rst

ENTRYPOINT ["aws"]
